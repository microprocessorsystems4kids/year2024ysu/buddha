#include <Arduino.h>
#include <U8g2lib.h>

// Pin the flap button is attached to
constexpr unsigned char FLAP_BUTTON = 2;
// Define the bird sprite dimensions
constexpr unsigned char SPRITE_HEIGHT = 16;
constexpr unsigned char  SPRITE_WIDTH  = 16;
// Game variables
constexpr unsigned char GAME_SPEED = 50;

// Two frames of animation
constexpr unsigned char PROGMEM wing_down_bmp[] = 
{
  B00000000, B00000000,
  B00000000, B00000000,
  B00000011, B11000000,
  B00011111, B11110000,
  B00111111, B00111000,
  B01111111, B11111110,
  B11111111, B11000001,
  B11011111, B01111110,
  B11011111, B01111000,
  B11011111, B01111000,
  B11001110, B01111000,
  B11110001, B11110000,
  B01111111, B11100000,
  B00111111, B11000000,
  B00000111, B00000000,
  B00000000, B00000000,
};

constexpr unsigned char PROGMEM wing_up_bmp[] = 
{
  B00000000, B00000000,
  B00000000, B00000000,
  B00000011, B11000000,
  B00011111, B11110000,
  B00111111, B00111000,
  B01110001, B11111110,
  B11101110, B11000001,
  B11011111, B01111110,
  B11011111, B01111000,
  B11111111, B11111000,
  B11111111, B11111000,
  B11111111, B11110000,
  B01111111, B11100000,
  B00111111, B11000000,
  B00000111, B00000000,
  B00000000, B00000000,
};

// Global object of the game
// Initialize the display (using U8g2 library)
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);

class Game {
  private:
    int score = 0; // current game score
    int high_score = 0; // highest score since the nano was reset
    int bird_x = 32; // birds x position (along) - initialised to 1/4 the way along the screen
    int bird_y; // birds y position (down)
    int wall_x[2]; // an array to hold the walls x positions
    int wall_y[2]; // an array to hold the walls y positions
    int wall_gap = 30; // gap between the walls

  public:
    Game(void);
    ~Game(void) = default;
    /* declaration of methods */
    void resetGame(void);
    void updateGame(void);
    void renderGame(void);
    /* public variables */
    int game_state; // 0 = game over screen, 1 = in game
    int momentum; // how much force is pulling the bird down
};

Game::Game(void) : game_state(1), momentum(0) {}

void Game::resetGame(void) {
  score = 0;
  bird_y = u8g2.getDisplayHeight() / 2;
  momentum = 0;
  wall_x[0] = u8g2.getDisplayWidth();
  wall_x[1] = u8g2.getDisplayWidth() + (u8g2.getDisplayWidth() / 2);
  wall_y[0] = random(10, u8g2.getDisplayHeight() - wall_gap - 10);
  wall_y[1] = random(10, u8g2.getDisplayHeight() - wall_gap - 10);
}

void Game::updateGame(void) {
  if (game_state == 1) {
    // Apply gravity
    momentum++;
    bird_y += momentum;

    // Move walls
    for (int i = 0; i < 2; i++) {
      wall_x[i] -= 2;
      if (wall_x[i] < -10) {
        wall_x[i] = u8g2.getDisplayWidth();
        wall_y[i] = random(10, u8g2.getDisplayHeight() - wall_gap - 10);
        score++;
        if (score > high_score) {
          high_score = score;
        }
        else { /* do nothing */ }
      }
      else { /* do nothing */ }
    }

    // Check for collisions
    for (int i = 0; i < 2; i++) {
      if (bird_x + SPRITE_WIDTH > wall_x[i] && bird_x < wall_x[i] + 10) {
        if (bird_y < wall_y[i] || bird_y + SPRITE_HEIGHT > wall_y[i] + wall_gap) {
          game_state = 0;
        }
        else { /* do nothing */ }
      }
      else { /* do nothing */ }
    }

    // Check if bird hits the ground or top
    if (bird_y > u8g2.getDisplayHeight() - SPRITE_HEIGHT || bird_y < 0) {
      game_state = 0;
    }
    else { /* do nothing */ }

  } 
  else {
    // Reset game if button is pressed
    if (digitalRead(FLAP_BUTTON) == LOW) {
      resetGame();
      game_state = 1;
    }
    else { /* do nothing */ }
  }
}

void Game::renderGame(void) {

  static bool wing_up = false;
  
  u8g2.clearBuffer();

  if (game_state == 1) {
    
    // Draw the bird
    wing_up = !wing_up;

    if (wing_up) {
      u8g2.drawXBM(bird_x, bird_y, SPRITE_WIDTH, SPRITE_HEIGHT, wing_up_bmp);
    } 
    else {
      u8g2.drawXBM(bird_x, bird_y, SPRITE_WIDTH, SPRITE_HEIGHT, wing_down_bmp);
    }

    // Draw the walls
    for (int i = 0; i < 2; i++) {
      u8g2.drawBox(wall_x[i], 0, 10, wall_y[i]);
      u8g2.drawBox(wall_x[i], wall_y[i] + wall_gap, 10, u8g2.getDisplayHeight() - wall_y[i] - wall_gap);
    }

    // Draw the score
    u8g2.setFont(u8g2_font_ncenB08_tr);
    u8g2.setCursor(0, 10);
    u8g2.print("Score: ");
    u8g2.print(score);
  } 
  else {
    // Draw the game over screen
    u8g2.setFont(u8g2_font_ncenB08_tr);
    u8g2.setCursor(20, 30);
    u8g2.print("Game Over");
    u8g2.setCursor(20, 50);
    u8g2.print("Score: ");
    u8g2.print(score);
    u8g2.setCursor(20, 60);
    u8g2.print("High Score: ");
    u8g2.print(high_score);
  }

  u8g2.sendBuffer();
}


void setup() {
  // Initialize display
  u8g2.begin();
  // Initialize button
  pinMode(FLAP_BUTTON, INPUT_PULLUP);
}

/* Program Loop */
void loop() {

  // Local object of the game
  Game game;

  // Initialize game state
  game.resetGame();

  /* game loop */
  for(;;) {

    // Read the button state
    int buttonState = digitalRead(FLAP_BUTTON);

    // Update game state based on button press
    if (buttonState == LOW && game.game_state == 1) {
      game.momentum = -5; // flap upwards
    }
    else { /* do nothing */ }

    // Update the game state
    game.updateGame();
    // Render the game
    game.renderGame();

    // Delay to control game speed
    delay(GAME_SPEED);
  }
}
